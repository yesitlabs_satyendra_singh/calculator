<?php
header("Access-Control-Allow-Origin: *");

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $file_name = $_POST['file_name'];
    $tip_weight = $_POST['tip_weight'];
    $draw_weight = $_POST['draw_weight'];
    $draw_length = $_POST['draw_length'];

    // Read Tip Weight
    $open = fopen($_SERVER['DOCUMENT_ROOT'] . "/csv/" . $file_name . "_tip.csv", "r");
    $column = fgetcsv($open);

    $row_no = 0;
    foreach ($column as $key => $value) {
        if ($value == $tip_weight) {
            $row_no = $key;
        }
    }

    $count = 0;
    while (($data = fgetcsv($open)) !== FALSE) {
        $arrayData = explode('-', $data[$row_no]);

        if ($draw_weight >= $arrayData[0] && $draw_weight <= $arrayData[1]) {
            break;
        }
        $count++;
    }

    fclose($open);

    // Read Arrow Length
    $open = fopen($_SERVER['DOCUMENT_ROOT'] . "/csv/" . $file_name . "_length.csv", "r");
    $column = fgetcsv($open);

    $row_no = 0;
    foreach ($column as $key => $value) {
        if ($value == $draw_length) {
            $row_no = $key;
        }
    }

    $spine = 0;
    $count_length = 0;
    while (($data = fgetcsv($open)) !== FALSE) {
        if ($count_length == $count) {
            $spine = $data[$row_no];
        }
        $count_length++;
    }

    fclose($open);

    echo $spine;
    exit;
}
