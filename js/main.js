$(document).ready(function () {
    var vane;
    var spine;
    var vane_no;
    var arrow_type;
    var shaft_length;
    var insert_type = 0.3;

    fetch('api.php').then(response => {
        return response.json();
    }).then(data => {
        let Arrow = [];
        let Vane_type = {};
        let Nock_weight = {};
        let Shaft_weight = {};
        let Vane_weight_length = {};

        data.arrow.forEach(value => {
            let shaftData = {};
            window[value.arrow] = [];

            Arrow.push(value.arrow);
            Nock_weight[value.arrow] = value.nock_weight;

            value.spineData.forEach(spine => {
                window[value.arrow].push(spine.spine);
                shaftData[spine.spine] = spine.shaft_weight;
            })

            Shaft_weight[value.arrow] = shaftData;

        });

        data.vane.forEach((value, index) => {
            Vane_type[index] = value.vane;
            Vane_weight_length[index] = { 'weight': value.vane_weight, 'length': value.vane_length };
        })

        // Render Arrow Type
        var option = `<option value="">-Select-</option>`;
        for (var i = 0; i < Arrow.length; i++) {
            option += `<option>` + Arrow[i] + `</option>`;
        }
        $('#arrow-type').html(option);

        $('#arrow-type').change(function () {
            arrow_type = $('#arrow-type').val();

            // Render Spine
            var option = `<option value="">-Select-</option>`;
            if ($.inArray(arrow_type, Arrow) >= 0) {
                for (var i = 0; i < window[arrow_type].length; i++) {
                    option += `<option>` + window[arrow_type][i] + `</option>`;
                }

                $('#nock_weight').val(Nock_weight[arrow_type]);
            }
            $('#spine').html(option);

            $('.reset-btn').click();
        })

        $('#spine').change(function () {
            spine = $('#spine').val();
            $('#shaft_weight').val(Shaft_weight[arrow_type][spine]);
            calculate_value();
        })

        // Render Vane Type
        var option = `<option value="">-Select-</option>`;
        for (var i = 0; i < data.vane.length; i++) {
            option += `<option value="` + i + `">` + Vane_type[i] + `</option>`;
        }
        $('#vane').html(option);

        $('#vane').change(function () {
            vane = $('#vane').val();
            if (vane == 10) {
                $('#vane_weight').attr('readonly', false);
                $('#vane_length').attr('readonly', false);
            } else {
                $('#vane_weight').attr('readonly', true);
                $('#vane_length').attr('readonly', true);
            }

            $('#vane_weight').val(Vane_weight_length[vane]['weight']);
            $('#vane_length').val(Vane_weight_length[vane]['length']);
            calculate_value();
        })
    })

    Shaft_length = { '1/8': '0.125', '1/4': '0.25', '3/8': '0.375', '1/2': '0.5', '5/8': '0.625', '3/4': '0.75', '7/8': '0.875' };

    var option = `<option value="">Select your desired length</option>`;
    for (var i = 24; i < 33; i++) {
        option += `<option value="` + i + `">` + i + ` INCHES</option>`;
        if (i == 32) break;

        var keys = Object.keys(Shaft_length);
        for (var j = 0; j < keys.length; j++) {
            option += `<option value="` + (i + parseFloat(Shaft_length[keys[j]])) + `">` + i + ` - ` + keys[j] + ` INCHES</option>`;
        }
    }
    $('#shaft-length').html(option);

    $('#shaft-length').change(function () {
        shaft_length = $('#shaft-length').val();
        calculate_value();
    })

    // Set Wrap Weight & Length
    $('#wrap').click(function () {
        if ($("#wrap").prop('checked') == true) {
            $('.wrap-box').show();
            $('#wrap_weight').val(6);
            $('#wrap_length').val(4);
        } else {
            $('.wrap-box').hide();
            $('#wrap_weight').val(0);
            $('#wrap_length').val(0);
        }
        calculate_value();
    })

    $('#vane_no').change(function () {
        vane_no = parseInt($('#vane_no').val());
        calculate_value();
    })

    $('#insert_type').change(function () {
        insert_type = $('#insert_type').val() ? parseFloat($('#insert_type').val()) : 0.3;
        calculate_value();
    })

    $('#form-calculate').submit(function (e) {
        e.preventDefault();
        calculate_value();
    })

    $('#point_weight').keyup(function () {
        calculate_value();
    })

    $('#insert_weight').keyup(function () {
        calculate_value();
    })

    function calculate_value() {
        var insert_weight = parseFloat($('#insert_weight').val());
        var point_weight = parseFloat($('#point_weight').val());
        var shaft_weight = parseFloat($('#shaft_weight').val());
        var wrap_weight = parseFloat($('#wrap_weight').val());
        var wrap_length = parseFloat($('#wrap_length').val());
        var vane_weight = parseFloat($('#vane_weight').val());
        var vane_length = parseFloat($('#vane_length').val());
        var nock_weight = parseFloat($('#nock_weight').val());

        if (vane_no > 0
            && point_weight > 0
            && shaft_weight > 0
            && vane_length > 0
            && nock_weight > 0
            && vane_weight > 0
            && insert_weight > 0
        ) {
            // Calculate Arrow Weigth
            var total_shaft_weight = shaft_weight * shaft_length;
            vane_weight = vane_weight * vane_no;
            var arrow_weight = insert_weight + point_weight + total_shaft_weight + wrap_weight + vane_weight + nock_weight;
            var weight_array = String(arrow_weight).split(".");

            if (weight_array.length > 1) {
                $('#arrow-weight').html(weight_array[0] + "." + weight_array[1].slice(0, 2) + " Grains");
            } else {
                $('#arrow-weight').html(weight_array[0] + " Grains");
            }

            // Calculate Arrow FOC
            var L_Vane = 1.25;
            var L_nock = 0.375;
            var point_length = 2.5;
            var L_Shaft = shaft_length / 2;
            var L_Component = parseFloat(shaft_length) - parseFloat(insert_type);
            var L_wrap = (wrap_length ? parseFloat(wrap_length) : 4) / 2;
            var L_Broadhead = parseFloat(shaft_length) + (parseFloat(point_length) / 2);

            var Adjusted_Shaft_Length = parseFloat(shaft_length) + parseFloat(L_nock) + 0.3;

            var Xcg = ((parseFloat(L_wrap) * parseFloat(wrap_weight)) + (parseFloat(L_Vane) * parseFloat(vane_weight)) + (parseFloat(L_Shaft) * parseFloat(total_shaft_weight)) + (parseFloat(L_Component) * parseFloat(insert_weight)) + (parseFloat(L_Broadhead) * parseFloat(point_weight)) - (parseFloat(L_nock) * parseFloat(nock_weight))) / parseFloat(arrow_weight);

            var FOC = 100 * ((Xcg - Adjusted_Shaft_Length / 2) / Adjusted_Shaft_Length);
            var foc_array = String(FOC).split(".");

            if (foc_array.length > 1) {
                $('#arrow-foc').html(foc_array[0] + "." + foc_array[1].slice(0, 2) + "%");
            } else {
                $('#arrow-foc').html(foc_array[0] + "%");
            }

            window.scroll(0, 300);
        }
    }

    $('.wrap-box').hide();

    $('.reset-btn').click(function () {
        $('#arrow-weight').html("0 Grains");
        $('#arrow-foc').html("0%");
    })
})

function digit_decimal(e) {
    console.log(e)
    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    var value = Number(e.target.value + e.key) || 0;

    if ((keyCode >= 48 && keyCode <= 57) || (keyCode == 46)) {
        return true;
    }
    return false;
}