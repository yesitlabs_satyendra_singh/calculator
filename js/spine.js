$(document).ready(function () {
    let speed = '';

    $('.infoicon').hide();
    $('#ibo_speed').hide();

    $('#bow_type').change(function () {
        let bow = $('#bow_type').val();

        if (bow == 'compound') {
            speed = 'above';
            $('.infoicon').show();
            $('#ibo_speed').show();
        } else {
            speed = '';
            $('.infoicon').hide();
            $('#ibo_speed').hide();
        }
    })

    $('#speed-1').click(function () {
        speed = 'above';
    })

    $('#speed-2').click(function () {
        speed = 'less';
    })

    // Draw Length
    let opt = '<option value="">Select Length</option>';
    for (let i = 25; i <= 32; i++) {
        opt += `<option value="` + i + `">` + i + `</option>`;
    }
    $('#draw_length').html(opt);

    $('#form-spine').submit(function (e) {
        e.preventDefault()

        let file_name = '';
        let bow_type = $('#bow_type').val();
        let tip_weight = $('#tip_weight').val();
        let draw_length = $('#draw_length').val();
        let draw_weight = $('#draw_weight').val();
        let point_weight = $('#point_weight').val();
        let insert_weight = $('#insert_weight').val();

        if (speed != '' && bow_type == 'compound') {
            file_name = bow_type + '_' + speed;
        } else {
            file_name = bow_type;
        }

        $.ajax({
            type: 'POST',
            data: { tip_weight: tip_weight, draw_weight: draw_weight, draw_length: draw_length, file_name: file_name },
            url: 'spine_api.php',
            success: function (response) {
                $('#spine_value').html(response);
                $('#exampleModal').modal('show');
            },
            error: function () {
                console.log('Something went wrong');
            }
        })
    })

    $('#btn-reset').click(function () {
        $('.infoicon').hide();
        $('#ibo_speed').hide();
    })
})

function digit_decimal(e) {
    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;

    if ((keyCode >= 48 && keyCode <= 57) || (keyCode == 46)) {
        return true;
    }
    return false;
}