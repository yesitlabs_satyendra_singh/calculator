<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Siriusarchery </title>
    <link rel="stylesheet" href="css/form-style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="js/main.js"></script>
</head>

<body>

    <section class="banner-sec">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-lg-6">
                    <div class="banner-text-box">
                        <h2>ARROW WEIGHT AND FOC (FORWARD OF CENTER BALANCE)</h2>
                        <p>Enter required information below to calculate your finished arrow weight and FOC.</p>
                        <p>FOC stands for Front of Center balance point. This measurement results from the relative
                            weights of the components used in the arrow: shaft, insert, point, fletching and nock. A
                            properly balanced arrow measurements of 7 to 15%. For more information on understanding
                            arrow weight and FOC.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="blackbg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <form id="form-calculate">
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <p>1. Arrow</p>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <select class="form-control" required id="arrow-type">
                                    <!-- Arrow Type -->
                                </select>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <p>2. Spine</p>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <select class="form-control" required id="spine">
                                    <!-- Spine -->
                                    <option value="">-Select-</option>
                                </select>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <p>3. Point Weight (GRAINS)</p>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="point_weight" onkeypress="return digit_decimal(event)">
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <p>4. Insert Weight (GRAINS)</p>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" required id="insert_weight" onkeypress="return digit_decimal(event)">
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <p>5. Insert Type</p>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <select class="form-control" id="insert_type">
                                    <option value="">-Select-</option>
                                    <option value="0.6">Standard Insert</option>
                                    <option value="0.3">Outsert/Sleeve System</option>
                                </select>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <p>6. Shaft Weight (GRAINS PER INCH)</p>
                            </div>
                            <div class="col-lg-2 col-md-2 col-5 short-tooltip">
                                <div class="form-group pos-relative">
                                    <input type="text" class="form-control" readonly required id="shaft_weight" onkeypress="return digit_decimal(event)">
                                    <div class="infoicon" data-toggle="tooltip" data-placement="right" title="Pre-filled based on the selection of spine. If needed, you can change the value based on your arrow. ">
                                        <img src="images/info.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <p>7. Nock Weight (GRAINS)</p>
                            </div>
                            <div class="col-lg-2 col-md-2 col-5 short-tooltip">
                                <div class="form-group pos-relative">
                                    <input type="text" class="form-control" required id="nock_weight" onkeypress="return digit_decimal(event)">
                                    <div class="infoicon" data-toggle="tooltip" data-placement="right" title="Pre-filled based on the selection of arrow. If needed, you can change the value based on your arrow. ">
                                        <img src="images/info.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <p>8. Carbon to Carbon Length (INCHES)</p>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <select class="form-control" id="shaft-length">
                                    <!-- Shaft Length -->
                                </select>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <p>9. Wrap ?</p>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <!-- partial:index.partial.html -->
                                <div class="can-toggle">
                                    <input id="wrap" type="checkbox">
                                    <label for="wrap">
                                        <div class="can-toggle__switch" data-unchecked="No" data-checked="Yes"></div>
                                    </label>
                                </div>
                                <!-- partial -->
                            </div>
                        </div>
                        <div class="row align-items-center wrap-box">
                            <div class="col-lg-6 col-md-6">
                                <p>Wrap Weight (GRAINS)</p>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <div class="form-group pos-relative">
                                    <input type="text" class="form-control" value="0" id="wrap_weight" onkeypress="return digit_decimal(event)">
                                    <div class="infoicon" data-toggle="tooltip" data-placement="right" title="Pre-filled based on the selection of wrap. If needed, you can change the value based on your arrow. ">
                                        <img src="images/info.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center wrap-box">
                            <div class="col-lg-6 col-md-6">
                                <p>Wrap Length (INCHES)</p>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <div class="form-group pos-relative">
                                    <input type="text" class="form-control" value="0" id="wrap_length" onkeypress="return digit_decimal(event)">
                                    <div class="infoicon" data-toggle="tooltip" data-placement="right" title="Pre-filled based on the selection of wrap. If needed, you can change the value based on your arrow. ">
                                        <img src="images/info.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <p>10. Vane</p>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <select class="form-control" required id="vane">
                                    <!-- Vane Type -->
                                </select>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <p>11. Vane Weight Per (GRAINS)</p>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <div class="form-group pos-relative">
                                    <input type="text" readonly class="form-control" required id="vane_weight" onkeypress="return digit_decimal(event)">
                                    <div class="infoicon" data-toggle="tooltip" data-placement="right" title="Pre-filled based on the selection of vane">
                                        <img src="images/info.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <p>12. Vane Length (INCHES)</p>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <div class="form-group pos-relative">
                                    <input type="text" readonly class="form-control" required id="vane_length" onkeypress="return digit_decimal(event)">
                                    <div class="infoicon" data-toggle="tooltip" data-placement="right" title="Pre-filled based on the selection of vane">
                                        <img src="images/info.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">
                                <p>13. Number Of Vanes</p>
                            </div>
                            <div class="col-lg-5 col-md-6">
                                <select class="form-control" required id="vane_no">
                                    <option value="">-Select-</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6">

                            </div>
                            <div class="col-lg-5 col-md-6">
                                <button type="reset" class="btn btn-calc">Clear</button>
                                <!-- <button type="submit" class="btn btn-calc">Calculate</button> -->
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4">
                    <div class="arrow-details-box">
                        <h5> Your Approx Arrow Weight is </h5>
                        <div class="calculated-point mb-5" id="arrow-weight"> 0 Grains </div>
                        <h5>Your Arrow FOC is </h5>
                        <div class="calculated-point" id="arrow-foc"> 0% </div>
                    </div>
                    <button class="reset-btn"> Reset </button>
                </div>
            </div>
        </div>
    </section>


    <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js'></script>
    <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'></script>
    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })

        $(function() {
            $('[data-toggle="popover"]').popover()
        })
    </script>
</body>

</html>