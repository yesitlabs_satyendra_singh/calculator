<!DOCTYPE html>

<html>



<head>

    <title>Siriusarchery | <?= ucwords($segment); ?></title>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Tell the browser to be responsive to screen width -->

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap 3.3.7 -->

    <link rel="stylesheet" href="<?= base_url('public') ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="<?= base_url('public') ?>/bower_components/font-awesome/css/font-awesome.min.css">

    <!-- Ionicons -->

    <link rel="stylesheet" href="<?= base_url('public') ?>/bower_components/Ionicons/css/ionicons.min.css">

    <!-- Theme style -->

    <link rel="stylesheet" href="<?= base_url('public') ?>/dist/css/AdminLTE.min.css">



    <link rel="stylesheet" href="<?= base_url('public') ?>/dist/css/skins/_all-skins.min.css">

    <!-- Date Picker -->

    <link rel="stylesheet" href="<?= base_url('public') ?>/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- Daterange picker -->

    <link rel="stylesheet" href="<?= base_url('public') ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">



    <link rel="stylesheet" href="<?= base_url('public') ?>/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <!-- Google Font -->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">



    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />

</head>



<body class="hold-transition skin-blue sidebar-mini">

    <div class="wrapper">



        <header class="main-header">

            <!-- Logo -->

            <a href="<?= base_url('admin/dashboard') ?>" class="logo">

                <!-- logo for regular state and mobile devices -->

                <span class="logo-lg"><b>Siriusarchery</b></span>

            </a>

            <!-- Header Navbar: style can be found in header.less -->

            <nav class="navbar navbar-static-top">

                <!-- Sidebar toggle button-->

                <a href="javascript:void(0)" class="sidebar-toggle" data-toggle="push-menu" role="button">

                    <span class="sr-only">Toggle navigation</span>

                </a>



                <div class="navbar-custom-menu">

                    <ul class="nav navbar-nav">

                        <!-- User Account: style can be found in dropdown.less -->

                        <li class="dropdown user user-menu">

                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">

                                <img src="<?= base_url('public') ?>/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">

                                <span class="hidden-xs"><?= $admin->name ?></span>

                            </a>

                            <ul class="dropdown-menu">

                                <!-- User image -->

                                <li class="user-header">

                                    <img src="<?= base_url('public') ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">



                                    <p>

                                        <?= $admin->name ?> - Administrator

                                    </p>

                                </li>

                                <!-- Menu Body -->



                                <!-- Menu Footer-->

                                <li class="user-footer">

                                    <div class="pull-left">

                                        <a href="<?= base_url('admin/profile') ?>" class="btn btn-default btn-flat">Profile</a>

                                    </div>

                                    <div class="pull-right">

                                        <a href="<?= base_url('admin/logout') ?>" class="btn btn-default btn-flat">Sign out</a>

                                    </div>

                                </li>

                            </ul>

                        </li>

                    </ul>

                </div>

            </nav>

        </header>