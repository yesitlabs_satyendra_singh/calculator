<footer class="main-footer text-center">

    <strong>Copyright &copy; 2021 <a href="<?= base_url() ?>">Siriusarchery</a>.</strong> All rights

    reserved.

</footer>



<!-- ./wrapper -->



<!-- jQuery 3 -->

<script src="<?= base_url('public') ?>/bower_components/jquery/dist/jquery.min.js"></script>



<!-- Datatables -->

<script src="<?= base_url('public') ?>/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?= base_url('public') ?>/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url('public') ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<!-- daterangepicker -->

<script src="<?= base_url('public') ?>/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url('public') ?>/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- datepicker -->

<script src="<?= base_url('public') ?>/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<!-- Slimscroll -->

<script src="<?= base_url('public') ?>/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->

<script src="<?= base_url('public') ?>/bower_components/fastclick/lib/fastclick.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url('public') ?>/dist/js/adminlte.min.js"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script src="<?= base_url('public') ?>/bower_components/ckeditor/ckeditor.js"></script>



<script>

    $(function() {

        toastr.options.closeButton = true;

        toastr.options.preventDuplicates = true;

        toastr.options.positionClass = "toast-top-right";



        <?php $this->session = \Config\Services::session();

        if ($this->session->success_msg) { ?>

            toastr.success("<?= $this->session->success_msg; ?>");

        <?php }

        if ($this->session->error_msg) { ?>

            toastr.error("<?= $this->session->error_msg; ?>");

        <?php } ?>

    });

</script>

<script>

    $(function() {

        $('.DataTable').DataTable();

    })

</script>

<script>

    $(function() {

        CKEDITOR.replace('editor')

    })

</script>

</body>



</html>