<aside class="main-sidebar">

    <section class="sidebar">

        <div class="user-panel">

            <div class="pull-left image">

                <img src="<?= base_url('public') ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

            </div>

            <div class="pull-left info">

                <p><?= ucfirst($admin->name) ?></p>

                <a href="<?= base_url('admin/profile') ?>"><i class="fa fa-circle text-success"></i> Online</a>

            </div>

        </div>



        <ul class="sidebar-menu" data-widget="tree">

            <li class="header">MAIN NAVIGATION</li>

            <li class="<?php if ($segment == 'dashboard') { echo 'active'; } ?>">

                <a href="<?= base_url('admin/dashboard') ?>">

                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>

                </a>

            </li>

            <li class="<?php if ($segment == "arrow" || $segment == "add arrow") { echo 'active'; } ?>">

                <a href="<?= base_url('admin/arrow') ?>">

                    <i class="fa fa-arrow-circle-o-up"></i> <span>Arrow Management</span>

                </a>

            </li>

            <li class="<?php if ($segment == "spine" || $segment == "add spine") { echo 'active'; } ?>">

                <a href="<?= base_url('admin/spine') ?>">

                    <i class="fa fa-th-list"></i> <span>Spine Management</span>

                </a>

            </li>

            <li class="<?php if ($segment == "vane" || $segment == "add vane") { echo 'active'; } ?>">

                <a href="<?= base_url('admin/vane') ?>">

                    <i class="fa fa-th-list"></i> <span>Vane Management</span>

                </a>

            </li>

        </ul>

    </section>

</aside>