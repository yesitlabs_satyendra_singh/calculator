<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Dashboard

            <small>Control panel</small>

        </h1>

        <ol class="breadcrumb">

            <li><a href="<?= base_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Dashboard</li>

        </ol>

    </section>



    <section class="content">

        <div class="row">

            <div class="col-lg-3 col-xs-6">

                <!-- small box -->

                <div class="small-box bg-aqua">

                    <div class="inner">

                        <h3><?= $arrow ?></h3>



                        <p>Arrows</p>

                    </div>

                    <div class="icon">

                        <i class="fa fa-arrow-circle-o-up"></i>

                    </div>

                    <a href="<?= base_url('admin/arrow') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                </div>

            </div>



            <div class="col-lg-3 col-xs-6">

                <!-- small box -->

                <div class="small-box bg-green">

                    <div class="inner">

                        <h3><?= $spine ?></h3>



                        <p>Spines</p>

                    </div>

                    <div class="icon">

                        <i class="fa fa-th-list"></i>

                    </div>

                    <a href="<?= base_url('admin/spine') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>

                </div>

            </div>

        </div>

    </section>

</div>