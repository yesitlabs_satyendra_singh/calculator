<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Add Spine
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Spine</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Spine</h3>
                    </div>

                    <form role="form" action="<?= base_url('admin/add_spine') ?>" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="arrow">Arrow</label>
                                <select name="arrow_id" class="form-control" id="arrow">
                                    <option value="">--Select Arrow--</option>
                                    <?php foreach ($arrow as $key => $value) { ?>
                                        <option value="<?= $value['id'] ?>"><?= $value['arrow'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="spine">Spine Value</label>
                                <input type="text" name="spine" class="form-control" id="spine" placeholder="Enter Spine">
                            </div>
                            <div class="form-group">
                                <label for="shaft_weight">Shaft Weight</label>
                                <input type="text" name="shaft_weight" class="form-control" id="shaft_weight" placeholder="Enter Shaft Weight">
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>