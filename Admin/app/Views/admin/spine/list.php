<?php

use App\Models\Arrow_model;

$arrow = new Arrow_model();
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Spine Management
            <small>Control panel</small>
        </h1>
        <div class="breadcrumb">
            <a href="<?= base_url('admin/add_spine') ?>">
                <button class="btn btn-success">Add New</button>
            </a>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-hover DataTable">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Arrow Name</th>
                                    <th>Spine</th>
                                    <th>Shaft Weight</th>
                                    <th>Date Time</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($spine)) {
                                    $count = 0;
                                    foreach ($spine as $key => $value) {
                                        $count++;
                                        $arrowData = $arrow->crud_read($value['arrow_id']);
                                ?>
                                        <tr>
                                            <td><?= $count ?></td>
                                            <td><?= $arrowData[0]['arrow'] ?></td>
                                            <td><?= $value['spine'] ?></td>
                                            <td><?= $value['shaft_weight'] ?></td>
                                            <td><?= date('d M, Y h:i A', strtotime($value['created_at'])) ?></td>
                                            <td>
                                                <a href="<?= base_url('admin/edit_spine') . '/' . $value['id']; ?>"><i class="fa fa-pencil"></i></a>&nbsp;
                                                <a onclick="return confirm('Are you sure, want to delete?')" href="<?= base_url('admin/delete_spine') . '/' . $value['id']; ?>"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                <?php }
                                } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
</div>