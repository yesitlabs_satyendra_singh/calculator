<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Add Arrow
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Arrow</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Arrow</h3>
                    </div>

                    <form role="form" action="<?= base_url('admin/add_arrow') ?>" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Arrow Name</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name">
                            </div>
                            <div class="form-group">
                                <label for="nock_weight">Nock Weight</label>
                                <input type="text" name="nock_weight" class="form-control" id="nock_weight" placeholder="Enter Nock Weight">
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>