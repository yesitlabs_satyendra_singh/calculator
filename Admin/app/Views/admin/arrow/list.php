<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Arrow Management
            <small>Control panel</small>
        </h1>
        <div class="breadcrumb">
            <a href="<?= base_url('admin/add_arrow') ?>">
                <button class="btn btn-success">Add New</button>
            </a>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-hover DataTable">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Arrow Name</th>
                                    <th>Nock Weight</th>
                                    <th>Date Time</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($arrow)) {
                                    $count = 0;
                                    foreach ($arrow as $key => $value) {
                                        $count++;
                                ?>
                                        <tr>
                                            <td><?= $count ?></td>
                                            <td><?= $value['arrow'] ?></td>
                                            <td><?= $value['nock_weight'] ?></td>
                                            <td><?= date('d M, Y h:i A', strtotime($value['created_at'])) ?></td>
                                            <td>
                                                <a href="<?= base_url('admin/edit_arrow') . '/' . $value['id']; ?>"><i class="fa fa-pencil"></i></a>&nbsp;
                                                <a onclick="return confirm('Are you sure, want to delete?')" href="<?= base_url('admin/delete_arrow') . '/' . $value['id']; ?>"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                <?php }
                                } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
</div>