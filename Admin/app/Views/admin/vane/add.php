<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Add Vane
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Vane</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Vane</h3>
                    </div>

                    <form role="form" action="<?= base_url('admin/add_vane') ?>" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Vane Name</label>
                                <input type="text" name="vane" class="form-control" id="name" placeholder="Enter Name">
                            </div>
                            <div class="form-group">
                                <label for="vane_weight">Vane Weight</label>
                                <input type="text" name="vane_weight" class="form-control" id="vane_weight" placeholder="Enter Vane Weight">
                            </div>
                            <div class="form-group">
                                <label for="vane_length">Vane Length</label>
                                <input type="text" name="vane_length" class="form-control" id="vane_length" placeholder="Enter Vane Length">
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>