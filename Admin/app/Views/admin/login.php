<!DOCTYPE html>

<html lang="en">



<head>

    <title>Login Page - Siriusarchery </title>



    <meta charset="utf-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <meta name="description" content="User login page" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />



    <!-- bootstrap & fontawesome -->

    <link rel="stylesheet" href="<?= base_url('public'); ?>/assets/css/bootstrap.min.css" />

    <link rel="stylesheet" href="<?= base_url('public'); ?>/assets/font-awesome/4.5.0/css/font-awesome.min.css" />



    <!-- text fonts -->

    <link rel="stylesheet" href="<?= base_url('public'); ?>/assets/css/fonts.googleapis.com.css" />



    <!-- ace styles -->

    <link rel="stylesheet" href="<?= base_url('public'); ?>/assets/css/ace.min.css" />

    <link rel="stylesheet" href="<?= base_url('public'); ?>/assets/css/ace-rtl.min.css" />

</head>



<body class="login-layout">

    <div class="main-container">

        <div class="main-content">

            <div class="row">

                <div class="col-sm-10 col-sm-offset-1">

                    <div class="login-container">

                        <div class="center">

                            <h4 class="blue" id="id-company-text">&copy; Siriusarchery</h4>

                        </div>



                        <div class="space-6"></div>



                        <div class="position-relative">

                            <div id="login-box" class="login-box visible widget-box no-border">

                                <div class="widget-body">

                                    <div class="widget-main">

                                        <h4 class="header blue lighter bigger">

                                            <i class="ace-icon fa fa-coffee green"></i>

                                            Please Enter Your Information

                                            <?php

                                            if (!empty($msg)) {

                                                echo '<div class="alert alert-danger" role="alert">' . $msg . '</div>';
                                            }

                                            ?>

                                        </h4>



                                        <div class="space-6"></div>



                                        <form action="<?= base_url('/admin/login_check'); ?>" method="post">

                                            <fieldset>

                                                <label class="block clearfix">

                                                    <span class="block input-icon input-icon-right">

                                                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter Your Email" required="">

                                                        <i class="ace-icon fa fa-user"></i>

                                                    </span>

                                                </label>



                                                <label class="block clearfix">

                                                    <span class="block input-icon input-icon-right">

                                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="">

                                                        <i class="ace-icon fa fa-lock"></i>

                                                    </span>

                                                </label>



                                                <div class="space"></div>



                                                <div class="clearfix">

                                                    <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">

                                                        <i class="ace-icon fa fa-key"></i>

                                                        <span class="bigger-110">Login</span>

                                                    </button>

                                                </div>



                                                <div class="space-4"></div>

                                            </fieldset>

                                        </form>

                                    </div>



                                </div>

                            </div>


                        </div>


                        <div class="navbar-fixed-top align-right">

                            <br />

                            &nbsp;

                            <a id="btn-login-dark" href="javascript:void(0)">Dark</a>

                            &nbsp;

                            <span class="blue">/</span>

                            &nbsp;

                            <a id="btn-login-blur" href="javascript:void(0)">Blur</a>

                            &nbsp;

                            <span class="blue">/</span>

                            &nbsp;

                            <a id="btn-login-light" href="javascript:void(0)">Light</a>

                            &nbsp; &nbsp; &nbsp;

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <script src="<?= base_url('public'); ?>/assets/js/jquery-2.1.4.min.js"></script>



    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement) document.write("<script src='<?= base_url('public'); ?>assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>



    <!-- inline scripts related to this page -->

    <script type="text/javascript">
        jQuery(function($) {

            $(document).on('click', '.toolbar a[data-target]', function(e) {

                e.preventDefault();

                var target = $(this).data('target');

                $('.widget-box.visible').removeClass('visible'); //hide others

                $(target).addClass('visible'); //show target

            });

        });



        // you don't need this, just used for changing background

        jQuery(function($) {

            $('#btn-login-dark').on('click', function(e) {

                $('body').attr('class', 'login-layout');

                $('#id-text2').attr('class', 'white');

                $('#id-company-text').attr('class', 'blue');



                e.preventDefault();

            });

            $('#btn-login-light').on('click', function(e) {

                $('body').attr('class', 'login-layout light-login');

                $('#id-text2').attr('class', 'grey');

                $('#id-company-text').attr('class', 'blue');



                e.preventDefault();

            });

            $('#btn-login-blur').on('click', function(e) {

                $('body').attr('class', 'login-layout blur-login');

                $('#id-text2').attr('class', 'white');

                $('#id-company-text').attr('class', 'light-blue');



                e.preventDefault();

            });



        });



        $(document).ready(function() {

            $('#btn-login-light').click();
            //get it if Status key found

            if (localStorage.getItem("Status")) {

                Toaster.show("Incorrect Email or Password.");

                localStorage.clear();

            }

        });
    </script>

</body>



</html>