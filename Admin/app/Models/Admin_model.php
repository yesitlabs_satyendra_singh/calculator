<?php

namespace App\Models;

use CodeIgniter\Model;

class Admin_model extends Model
{
    protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('admin');
    }

    /**
     * Admin Login
     * @param   array   $data
     * @return  array
     */
    function admin_verify($email, $password)
    {
        $this->builder->where('email', $email);
        $this->builder->where('password', md5($password));
        return $this->builder->get()->getRow();
    }

    /**
     * Update Profile
     * @param   array   $data
     * @param   int     $id
     * @return  bool
     */
    function crud_update($data, $id)
    {
        $this->builder->where('id', $id);
        return $this->builder->update($data);
    }

    /**
     * Read Profile
     * @param   int $id
     * @return  array
     */
    function crud_read($id)
    {
        $this->builder->where('id', $id);
        return $this->builder->get()->getRow();
    }
}
