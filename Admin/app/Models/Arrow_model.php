<?php



namespace App\Models;



use CodeIgniter\Model;



class Arrow_model extends Model

{

    protected $db;



    public function __construct()

    {

        $this->db = \Config\Database::connect();

        $this->builder = $this->db->table('tbl_arrow');
        $this->builder1 = $this->db->table('tbl_spine');
    }

    /**
     * Create Arrow
     * @param   array   $data
     * @return  bool
     */
    function crud_create($data)
    {
        $this->builder->insert($data);
        return $this->db->insertID();
    }


    /**

     * Update Arrow

     * @param   array   $data

     * @param   int     $id

     * @return  bool

     */

    function crud_update($data, $id)

    {

        $this->builder->where('id', $id);

        return $this->builder->update($data);
    }



    /**

     * Read Arrow

     * @param   int $id

     * @return  array

     */

    function crud_read($id = '')

    {
        if ($id) {
            $this->builder->where('id', $id);
        }

        return $this->builder->get()->getResultArray();
    }

    /**

     * Delete Arrow

     * @param   int $id

     * @return  bool

     */

    function crud_delete($id)

    {

        $this->builder->where('id', $id);
        $this->builder1->where('arrow_id', $id);

        $this->builder->delete();
        $this->builder1->delete();
        return true;
    }
}
