<?php



namespace App\Models;



use CodeIgniter\Model;



class Vane_model extends Model

{

    protected $db;



    public function __construct()

    {

        $this->db = \Config\Database::connect();

        $this->builder = $this->db->table('tbl_vane');
    }

    /**
     * Create Vane
     * @param   array   $data
     * @return  bool
     */
    function crud_create($data)
    {
        $this->builder->insert($data);
        return $this->db->insertID();
    }


    /**

     * Update Vane

     * @param   array   $data

     * @param   int     $id

     * @return  bool

     */

    function crud_update($data, $id)

    {

        $this->builder->where('id', $id);

        return $this->builder->update($data);
    }



    /**

     * Read Vane

     * @param   int $id

     * @return  array

     */

    function crud_read($cond = '')

    {
        if ($cond) {
            $this->builder->where($cond);
        }

        return $this->builder->get()->getResultArray();
    }

    /**

     * Delete Vane

     * @param   int $id

     * @return  bool

     */

    function crud_delete($id)

    {

        $this->builder->where('id', $id);

        return $this->builder->delete();
    }
}
