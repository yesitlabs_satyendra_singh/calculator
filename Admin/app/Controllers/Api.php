<?php



namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;


class Api extends BaseController

{

    use ResponseTrait;

    /**

     * JSON Response 

     * @param   array   $data

     * @return  string

     */

    private function response($data, $status = 200)

    {

        header('Content-Type: application/json');

        echo json_encode($data);

        exit;
    }

    public function index()

    {

        $returndata = array(

            "message" => "Arrow FOC API"

        );

        return $this->response($returndata);
    }
}
