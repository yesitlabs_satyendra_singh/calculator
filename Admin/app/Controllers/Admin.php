<?php

namespace App\Controllers;

use App\Models\Vane_model;
use App\Models\Arrow_model;
use App\Models\Spine_model;
use App\Models\Admin_model;

class Admin extends BaseController

{

    public function __construct()

    {

        $this->session = session();
    }


    /**

     * Check Login

     */

    private function checkLogin()

    {

        if (!$this->session->get('adminData')) {

            header("location: " . base_url('admin') . "/login");

            exit;
        }

        return $this->session->get('adminData');
    }



    /**

     * Load View

     * @param   string  $viewname

     * @param   array   $data

     */

    private function loadView($viewname, $data = array())

    {

        $segments = $this->request->uri->getSegments();

        $segment['admin'] = $this->checkLogin();



        if (isset($segments[1])) {

            $segment['segment'] = str_replace(['_', '-'], ' ', $segments[1]);
        } else {

            $segment['segment'] = '';
        }



        echo view("include/header", $segment);

        echo view("include/sidebar");

        echo view("admin/$viewname", $data);

        echo view("include/footer");
    }



    /**

     * Send Mail

     * @param   string  $to

     * @param   string  $subject

     * @param   string  $message

     * @return  bool

     */

    private function sendMail($to, $subject, $message)

    {

        return mail($to, $subject, $message, array("from" => "support@calculator.com"));
    }



    public function index()

    {

        $this->checkLogin();

        return redirect()->to(base_url('admin/dashboard'));
    }



    public function login()

    {

        return view('admin/login');
    }



    public function login_check()

    {

        $login = new Admin_model();



        if (!empty($this->request->getPost('email'))) {

            $adminData = $login->admin_verify(

                $this->request->getPost('email'),

                $this->request->getPost('password')

            );



            if (!empty($adminData)) {

                $this->session->set('adminData', $adminData);

                return redirect()->to(base_url('admin/dashboard'));
            } else {

                $data['msg'] = "Incorrect Email or Password";

                return view('admin/login', $data);
            }
        }
    }



    public function logout()

    {

        $this->session->remove('adminData');

        return redirect()->to(base_url('admin/login'));
    }



    public function dashboard()

    {
        $arrow = new Arrow_model();
        $spine = new Spine_model();


        $data['arrow'] = count($arrow->crud_read());

        $data['spine'] = count($spine->crud_read());

        $this->loadView('dashboard', $data);
    }



    public function profile()

    {

        $admin = new Admin_model();



        if ($this->request->getPost()) {

            $data   = array();

            $name   = $this->request->getPost('name');

            $email  = $this->request->getPost('email');

            $npass  = $this->request->getPost('npass');

            $cpass  = $this->request->getPost('cpass');



            if (!empty($npass) && !empty($cpass)) {

                if ($npass == $cpass) {

                    $data['password'] = md5($npass);
                } else {

                    $this->session->setFlashdata('error_msg', "Password not matched.");

                    return redirect()->to(base_url('admin/profile'));
                }
            }



            if (!empty($name) && !empty($email)) {

                $data['name'] = $name;

                $data['email'] = $email;
            }



            if (!empty($data)) {

                $admin->crud_update($data, $this->session->get('adminData')->id);

                $this->session->set('adminData', $admin->crud_read($this->session->get('adminData')->id));



                $this->session->setFlashdata('success_msg', "Profile successfully updated.");

                return redirect()->to(base_url('admin/profile'));
            } else {

                $this->session->setFlashdata('error_msg', "Profile not updated.");

                return redirect()->to(base_url('admin/profile'));
            }
        } else {

            $this->loadView('profile');
        }
    }

    public function arrow()
    {
        $arrow = new Arrow_model();
        $data['arrow'] = $arrow->crud_read();

        $this->loadView('arrow/list', $data);
    }

    public function add_arrow()
    {
        $arrow = new Arrow_model();

        if ($this->request->getPost()) {
            $data['arrow'] = $this->request->getPost('name');
            $data['nock_weight'] = $this->request->getPost('nock_weight');
            $data['created_at'] = date('Y-m-d H:i:s');

            if (!empty($data['arrow'])) {
                $result = $arrow->crud_create($data);

                if ($result) {
                    $this->session->setFlashdata('success_msg', "Arrow successfully added.");

                    return redirect()->to(base_url('admin/arrow'));
                } else {
                    $this->session->setFlashdata('error_msg', "Arrow not added.");

                    return redirect()->to(base_url('admin/arrow'));
                }
            } else {
                $this->session->setFlashdata('error_msg', "Please provide all required fields.");
                return redirect()->to(base_url('admin/add_arrow'));
            }
        } else {
            $this->loadView('arrow/add');
        }
    }

    public function edit_arrow($id)
    {
        $arrow = new Arrow_model();

        if ($this->request->getPost()) {
            $data['arrow'] = $this->request->getPost('name');
            $data['nock_weight'] = $this->request->getPost('nock_weight');

            if (!empty($data['arrow'])) {
                $result = $arrow->crud_update($data, $id);

                if ($result) {
                    $this->session->setFlashdata('success_msg', "Arrow successfully updated.");

                    return redirect()->to(base_url('admin/arrow'));
                } else {
                    $this->session->setFlashdata('error_msg', "Arrow not updated.");

                    return redirect()->to(base_url('admin/arrow'));
                }
            } else {
                $this->session->setFlashdata('error_msg', "Please provide all required fields.");
                return redirect()->to(base_url('admin/add_arrow'));
            }
        } else {
            $data['arrow'] = $arrow->crud_read($id);
            $this->loadView('arrow/edit', $data);
        }
    }

    public function delete_arrow($id)
    {
        $arrow = new Arrow_model();
        $result = $arrow->crud_delete($id);

        if ($result) {
            $this->session->setFlashdata('success_msg', "Arrow successfully deleted.");

            return redirect()->to(base_url('admin/arrow'));
        } else {
            $this->session->setFlashdata('error_msg', "Arrow not deleted.");

            return redirect()->to(base_url('admin/arrow'));
        }
    }

    public function spine()
    {
        $spine = new Spine_model();
        $data['spine'] = $spine->crud_read();
        $this->loadView('spine/list', $data);
    }

    public function add_spine()
    {
        $arrow = new Arrow_model();
        $spine = new Spine_model();

        if ($this->request->getPost()) {
            $data['spine'] = $this->request->getPost('spine');
            $data['arrow_id'] = $this->request->getPost('arrow_id');
            $data['shaft_weight'] = $this->request->getPost('shaft_weight');
            $data['created_at'] = date('Y-m-d H:i:s');

            if (!empty($data['spine']) && !empty($data['arrow_id'])) {
                $result = $spine->crud_create($data);

                if ($result) {
                    $this->session->setFlashdata('success_msg', "Spine successfully added.");

                    return redirect()->to(base_url('admin/spine'));
                } else {
                    $this->session->setFlashdata('error_msg', "Spine not added.");

                    return redirect()->to(base_url('admin/spine'));
                }
            } else {
                $this->session->setFlashdata('error_msg', "Please provide all required fields.");
                return redirect()->to(base_url('admin/add_spine'));
            }
        } else {
            $data['arrow'] = $arrow->crud_read();
            $this->loadView('spine/add', $data);
        }
    }

    public function edit_spine($id)
    {
        $arrow = new Arrow_model();
        $spine = new Spine_model();

        if ($this->request->getPost()) {
            $data['spine'] = $this->request->getPost('spine');
            $data['arrow_id'] = $this->request->getPost('arrow_id');
            $data['shaft_weight'] = $this->request->getPost('shaft_weight');

            if (!empty($data['spine']) && !empty($data['arrow_id'])) {
                $result = $spine->crud_update($data, $id);

                if ($result) {
                    $this->session->setFlashdata('success_msg', "Spine successfully updated.");

                    return redirect()->to(base_url('admin/spine'));
                } else {
                    $this->session->setFlashdata('error_msg', "Spine not updated.");

                    return redirect()->to(base_url('admin/spine'));
                }
            } else {
                $this->session->setFlashdata('error_msg', "Please provide all required fields.");
                return redirect()->to(base_url('admin/add_spine'));
            }
        } else {
            $data['arrow'] = $arrow->crud_read();
            $data['spine'] = $spine->crud_read(['id' => $id]);
            $this->loadView('spine/edit', $data);
        }
    }

    public function delete_spine($id)
    {
        $spine = new Spine_model();
        $result = $spine->crud_delete($id);

        if ($result) {
            $this->session->setFlashdata('success_msg', "Spine successfully deleted.");

            return redirect()->to(base_url('admin/spine'));
        } else {
            $this->session->setFlashdata('error_msg', "Spine not deleted.");

            return redirect()->to(base_url('admin/spine'));
        }
    }

    public function vane()
    {
        $vane = new Vane_model();
        $data['vane'] = $vane->crud_read();
        $this->loadView('vane/list', $data);
    }

    public function add_vane()
    {
        $vane = new Vane_model();

        if ($this->request->getPost()) {
            $data['vane'] = $this->request->getPost('vane');
            $data['vane_length'] = $this->request->getPost('vane_length');
            $data['vane_weight'] = $this->request->getPost('vane_weight');
            $data['created_at'] = date('Y-m-d H:i:s');

            if (!empty($data['vane']) && !empty($data['vane_length'])) {
                $result = $vane->crud_create($data);

                if ($result) {
                    $this->session->setFlashdata('success_msg', "Vane successfully added.");

                    return redirect()->to(base_url('admin/vane'));
                } else {
                    $this->session->setFlashdata('error_msg', "Vane not added.");

                    return redirect()->to(base_url('admin/vane'));
                }
            } else {
                $this->session->setFlashdata('error_msg', "Please provide all required fields.");
                return redirect()->to(base_url('admin/add_vane'));
            }
        } else {
            $data['vane'] = $vane->crud_read();
            $this->loadView('vane/add', $data);
        }
    }

    public function edit_vane($id)
    {
        $vane = new Vane_model();

        if ($this->request->getPost()) {
            $data['vane'] = $this->request->getPost('vane');
            $data['vane_length'] = $this->request->getPost('vane_length');
            $data['vane_weight'] = $this->request->getPost('vane_weight');

            if (!empty($data['vane']) && !empty($data['vane_length'])) {
                $result = $vane->crud_update($data, $id);

                if ($result) {
                    $this->session->setFlashdata('success_msg', "Vane successfully updated.");

                    return redirect()->to(base_url('admin/vane'));
                } else {
                    $this->session->setFlashdata('error_msg', "Vane not updated.");

                    return redirect()->to(base_url('admin/vane'));
                }
            } else {
                $this->session->setFlashdata('error_msg', "Please provide all required fields.");
                return redirect()->to(base_url('admin/add_vane'));
            }
        } else {
            $data['vane'] = $vane->crud_read();
            $this->loadView('vane/edit', $data);
        }
    }

    public function delete_vane($id)
    {
        $vane = new Vane_model();
        $result = $vane->crud_delete($id);

        if ($result) {
            $this->session->setFlashdata('success_msg', "Vane successfully deleted.");

            return redirect()->to(base_url('admin/vane'));
        } else {
            $this->session->setFlashdata('error_msg', "Vane not deleted.");

            return redirect()->to(base_url('admin/vane'));
        }
    }
}
