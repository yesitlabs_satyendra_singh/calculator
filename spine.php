<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> Siriusarchery | Spine </title>
	<link rel="stylesheet" href="spine/form-style.css">
	<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
	<script src="js/spine.js"></script>
</head>

<body>
	<section class="banner-sec">
		<div class="container">
			<div class="row justify-content-end">
				<div class="col-lg-6">
					<div class="banner-text-box next-txt">
						<h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h2>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="blackbg">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6">
					<form id="form-spine">
						<div class="row align-items-center">
							<div class="col-lg-4 col-md-4">
								<p>Bow Type</p>
							</div>
							<div class="col-lg-8 col-md-8 pos-relative col-11">
								<select class="form-control" required id="bow_type">
									<option value="">Select Bow</option>
									<option value="compound">Compound</option>
									<option value="recurve">Recurve</option>
								</select>
								<div class="infoicon" data-toggle="tooltip" data-placement="right" title="if the IBO speed of the bow is above or below 340 Ft/s.">
									<img src="images/info.png">
								</div>
							</div>
						</div>
						<div class="row align-items-center" id="ibo_speed">
							<div class="col-lg-4 col-md-4">
								<p class="sub-points">Choose IBO Speed</p>
							</div>
							<div class="col-lg-8 col-md-8">
								<label class="check-cont">Above 340 Ft/s
									<input type="radio" id="speed-1" checked="checked" name="radio">
									<span class="checkmark"></span>
								</label>
								<label class="check-cont">Below 340 Ft/s
									<input type="radio" id="speed-2" name="radio">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<div class="row align-items-center">
							<div class="col-lg-4 col-md-4">
								<p>Draw Length</p>
							</div>
							<div class="col-lg-8 col-md-8">
								<select class="form-control" required id="draw_length">
									<!-- Draw Length -->
								</select>
							</div>
						</div>
						<div class="row align-items-center">
							<div class="col-lg-4 col-md-4">
								<p>Draw Weight</p>
							</div>
							<div class="col-lg-8 col-md-8">
								<div class="form-group">
									<input type="text" required class="form-control" id="draw_weight" onkeypress="return digit_decimal(event)">
								</div>
							</div>
						</div>
						<div class="row align-items-center">
							<div class="col-lg-4 col-md-4">
								<p>Tip Weight </p>
							</div>
							<div class="col-lg-8 col-md-8">
								<div class="form-group">
									<input type="text" required class="form-control" id="tip_weight" onkeypress="return digit_decimal(event)">
								</div>
							</div>
						</div>
						<div class="row align-items-center">
							<div class="col-lg-4 col-md-4">
								<p class="sub-points">Point Weight</p>
							</div>
							<div class="col-lg-8 col-md-8 col-12">
								<input type="text" class="form-control" required id="point_weight" onkeypress="return digit_decimal(event)">
							</div>
						</div>
						<div class="row align-items-center">
							<div class="col-lg-4 col-md-4">
								<p class="sub-points">Insert Weight</p>
							</div>
							<div class="col-lg-8 col-md-8 col-12">
								<input type="text" class="form-control" required id="insert_weight" onkeypress="return digit_decimal(event)">
							</div>
						</div>
						<div class="row align-items-center">
							<div class="col-lg-6 col-md-6">

							</div>
							<div class="col-lg-5 col-md-6">
								<button type="reset" id="btn-reset" class="btn btn-calc">Clear</button>
								<button type="submit" class="btn btn-calc">Calculate</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header-new">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="plz-call">
						<p>Your recommended spine is <b id="spine_value"></b>.<br> If you have any questions, please call 877-442-7769 Ext. 1. Thank you! </p>
					</div>
					<div class="btn-center">
						<div class="ok-modal-btn" data-dismiss="modal" aria-label="Close">OK</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js'></script>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'></script>
<script>
	$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	})

	$(function() {
		$('[data-toggle="popover"]').popover()
	})
</script>

</html>